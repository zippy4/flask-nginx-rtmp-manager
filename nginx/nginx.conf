user  www-data;
worker_processes  1;

pid        /run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;
    server {
        listen       80;
        server_name  localhost;
        # set client body size to 16M #
        client_max_body_size 16M;

        location / {
            proxy_pass http://127.0.0.1:5000;
        }

        location /socket.io {
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host;
                proxy_set_header X-NginX-Proxy true;

                # prevents 502 bad gateway error
                proxy_buffers 8 32k;
                proxy_buffer_size 64k;

                proxy_redirect off;

                # enables WS support
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";

                proxy_pass http://127.0.0.1:5000/socket.io;

        }

        location ~ /live(.*) {
                # Disable cache
                add_header Cache-Control no-cache;

                # CORS setup
                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Expose-Headers' 'Content-Length';

                # allow CORS preflight requests
                if ($request_method = 'OPTIONS') {
                        add_header 'Access-Control-Allow-Origin' '*';
                        add_header 'Access-Control-Max-Age' 1728000;
                        add_header 'Content-Type' 'text/plain charset=UTF-8';
                        add_header 'Content-Length' 0;
                        return 204;
                }

                types {
                        application/vnd.apple.mpegurl m3u8;
                        video/mp2t ts;
                }

                root /var/www/;
        }

        location ~ /videos(.*) {
                # Disable cache
                add_header Cache-Control no-cache;

                # CORS setup
                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Expose-Headers' 'Content-Length';

                # allow CORS preflight requests
                if ($request_method = 'OPTIONS') {
                        add_header 'Access-Control-Allow-Origin' '*';
                        add_header 'Access-Control-Max-Age' 1728000;
                        add_header 'Content-Type' 'text/plain charset=UTF-8';
                        add_header 'Content-Length' 0;
                        return 204;
                }

                types {
                        application/vnd.apple.mpegurl m3u8;
                        video/mp2t ts;
                }

                root /var/www;
        }

        location ~ /images(.*) {
                # Disable cache
                add_header Cache-Control no-cache;

                # CORS setup
                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Expose-Headers' 'Content-Length';

                # allow CORS preflight requests
                if ($request_method = 'OPTIONS') {
                        add_header 'Access-Control-Allow-Origin' '*';
                        add_header 'Access-Control-Max-Age' 1728000;
                        add_header 'Content-Type' 'text/plain charset=UTF-8';
                        add_header 'Content-Length' 0;
                        return 204;
                }

                types {
                        application/vnd.apple.mpegurl m3u8;
                        video/mp2t ts;
                }

                root /var/www;
        }

        # redirect server error pages to the static page /50x.html
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}

rtmp {
        server {
                listen 1935;
                chunk_size 4096;

                application stream {
                        live on;
                        record off;

                        allow publish all;
                        #deny publish all;
                        allow play all;

                        on_publish http://127.0.0.1:5000/auth-key;
                        on_publish_done http://127.0.0.1:5000/deauth-user;

                }
                application stream-data {
                        live on;

                        allow publish all;
                        #deny publish all;
                        allow play all;

                        on_publish http://127.0.0.1:5000/auth-user;

                        hls on;
                        hls_path /var/www/live;
                        hls_fragment 3;

                        hls_nested on;
                        hls_fragment_naming system;

                        recorder thumbnail {
                            record keyframes;
                            record_max_frames 4;
                            record_path /var/www/live;
                            record_interval 30s;

                            exec_record_done ffmpeg -i $path -vcodec png -vframes 1 -an -f rawvideo -s 384x216 -ss 00:00:01 -y /var/www/live/$name.png;
                        }

                }
                application streamrec-data {
                        live on;

                        allow publish all;
                        #deny publish all;
                        allow play all;

                        on_publish http://127.0.0.1:5000/auth-user;

                        hls on;
                        hls_path /var/www/live-rec;
                        hls_fragment 3;

                        hls_nested on;
                        hls_fragment_naming system;

                        exec_push mkdir -m 764 /var/www/videos/$name;

                        recorder all {
                            record all;
                            record_path /tmp;
                            #record_max_size 100000K;
                            record_unique on;
                            record_suffix _%Y%m%d_%H%M%S.flv;
                            exec_record_done ffmpeg -y -i $path -codec copy -movflags +faststart /var/www/videos/$name/$basename.mp4;
                            exec_record_done mv /var/www/live-rec/$name.png /var/www/videos/$name/$basename.png;
                            on_record_done http://127.0.0.1:5000/recComplete;
                        }

                        recorder thumbnail {
                            record keyframes;
                            record_max_frames 4;
                            record_path /var/www/live-rec;
                            record_interval 30s;

                            exec_record_done ffmpeg -i $path -vcodec png -vframes 1 -an -f rawvideo -s 384x216 -ss 00:00:01 -y /var/www/live-rec/$name.png;
                        }

                }

        }
}